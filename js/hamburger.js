$(document).ready(function () {

    var menuOpen = 0; /*variable that changes between 0 and 1 each time the button is clicked*/

    $('.button').click(function () {

        if (menuOpen === 0) {
            $('#hamburger').removeClass('hide-menu');
            $('#hamburger').addClass('show-menu'); /*add the class that activates the hamburger slide out animation*/
            menuOpen = 1; /*change the value of 'menuOpen' so that when the user next clicks on the button, the code in the else if is executed*/
        }

        else if (menuOpen === 1) {
            $('#hamburger').removeClass('show-menu');
            $('#hamburger').addClass('hide-menu'); /*add class the activates the hamburger slide in animation*/
            menuOpen = 0;
        }
    });

});